Source: nitime
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Yaroslav Halchenko <debian@onerussian.com>,
           Michael Hanke <mih@debian.org>,
           Nilesh Patra <nilesh@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               python3-all,
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-numpy,
               python3-scipy,
               python3-matplotlib,
               python3-tk,
               python3-sphinx,
               python3-networkx,
               python3-nibabel,
               python3-setuptools,
               python3-setuptools-scm,
               python3-pytest,
               graphviz
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/nitime
Vcs-Git: https://salsa.debian.org/med-team/nitime.git
Homepage: https://nipy.org/nitime
Rules-Requires-Root: no

Package: python3-nitime
Architecture: all
Depends: ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends},
         python3-numpy,
         python3-scipy
Recommends: python3-matplotlib,
            python3-nibabel,
            python3-networkx
Description: timeseries analysis for neuroscience data (nitime)
 Nitime is a Python module for time-series analysis of data from
 neuroscience experiments.  It contains a core of numerical algorithms
 for time-series analysis both in the time and spectral domains, a set
 of container objects to represent time-series, and auxiliary objects
 that expose a high level interface to the numerical machinery and
 make common analysis tasks easy to express with compact and
 semantically clear code.

Package: python-nitime-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         libjs-jquery,
         libjs-underscore
Suggests: python3-nitime
Multi-Arch: foreign
Description: timeseries analysis for neuroscience data (nitime) -- documentation
 Nitime is a Python module for time-series analysis of data from
 neuroscience experiments.
 .
 This package provides the documentation in HTML format.
