## To run the test:

`sh run-unit-test`

The package needs be built from scratch to run the upstream test.

Upstream test can be run with:

`$py -m pytest`

However, this will popup two GUIS which needs to be closed before tests run.

The path to each test folder was specified to circumvent the GUI popup.