nitime (0.10.2-2) UNRELEASED; urgency=medium

  * Added autopkgtest (Closes: #1058705)
  
 -- Komolehin Israel Timilehin <komolehinisrael@gmail.com>  Wed, 20 Dec 2023 12:19:23 +0000

nitime (0.10.2-1) unstable; urgency=medium

  [ Andreas Tille ]
  * New upstream version  (Closes: #1058416)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * dh_clean needs more adjustments

  [ Étienne Mollier ]
  * d/control: build depends on python3-setuptools-scm.
  * sphinx-conf-fix.patch: new: fix conf.py.
    This is necessary as doc/conf.py still relies on the old
    nitime/version.py, which has been replaced by nitime/_version.py,
    causing various issues when trying to build python-nitime-doc.
  * d/python-nitime-doc.lintian-overrides: refresh.
    This fixes a mismatched override about embedded javascript.
  * d/control: depend on pyproject backend.
  * demote-cython.patch: new: do not depend on cython3. (Closes: #1057996)
  * d/control: add myself to uploaders.

 -- Étienne Mollier <emollier@debian.org>  Thu, 14 Dec 2023 22:51:37 +0100

nitime (0.10.1-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Drop debian/blends which is unused

  [ Étienne Mollier ]
  * New upstream version 0.10.1  (Closes: #1042247)
  * do-not-set-lowerbound-zero-in-iir.patch: delete: fixed upstream.
  * fix-psd-test.patch: delete: fixed upstream.
  * numpy_1.24.patch: remove: applied upstream.

  [ Nilesh Patra ]
  * Use deprecated networkx function for now, as the drop-in replacement
    upstream used is causing a regression.
    Proper Fix suggested upstream already.
  * Add patch to fix doc FTBFS with new matplotlib

 -- Nilesh Patra <nilesh@debian.org>  Fri, 25 Aug 2023 01:55:48 +0530

nitime (0.9-5) unstable; urgency=medium

  [ Andreas Tille ]
  * Adapt to numpy 1.24 (Closes: #1029245)

  [ Nilesh Patra ]
  * Actually and genuinely add a patch for numpydoc errors

 -- Nilesh Patra <nilesh@debian.org>  Sat, 21 Jan 2023 10:28:38 +0530

nitime (0.9-4) unstable; urgency=medium

  * Add patch to fix test_psd_matlab (Closes: #1027550)
  * docs/conf.py: Look in axes_grid1 instead of axes_grid
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Mon, 02 Jan 2023 00:14:51 +0530

nitime (0.9-3) unstable; urgency=medium

  * Add patch to not set ws lower bound to zero (Closes: #1013569)

 -- Nilesh Patra <nilesh@debian.org>  Mon, 11 Jul 2022 18:30:08 +0530

nitime (0.9-2) unstable; urgency=medium

  * Switch to pytest instead of nose

 -- Nilesh Patra <nilesh@debian.org>  Mon, 25 Oct 2021 02:30:56 +0530

nitime (0.9-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix watchfile to detect new versions on github (routine-update)
  * Standards-Version: 4.6.0 (routine-update)
  * Apply multi-arch hints.
    + python-nitime-doc: Add Multi-Arch: foreign.

 -- Andreas Tille <tille@debian.org>  Wed, 01 Sep 2021 16:43:09 +0200

nitime (0.9-1) unstable; urgency=medium

  * Add myself to uploaders
  * New upstream version 0.9
  * Remove merged patch
  * Standards-Version: 4.5.1 (routine-update)
  * Switch watch version to 4
  * Do not mention removed file in copyright

 -- Nilesh Patra <npatra974@gmail.com>  Sun, 20 Dec 2020 20:20:57 +0530

nitime (0.8.1-4) unstable; urgency=medium

  * Team Upload.
  * Fix according changed sphinx API (Closes: #963663)
  * Remove ajax inclusion in html files
  * Fix lintian
  * debhelper-compat 13 (routine-update)
  * Update upstream/metadata

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 22 Aug 2020 02:34:08 +0530

nitime (0.8.1-3) unstable; urgency=medium

  * Team upload.
  * Ignore github extension since it is not compatible with Shinx 2.4
    Closes: #955103
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 16 Apr 2020 11:45:12 +0200

nitime (0.8.1-2) unstable; urgency=medium

  * Team upload.
  * Reupload source package to enable testing migration
  * Set upstream metadata fields: Bug-Submit.

 -- Andreas Tille <tille@debian.org>  Thu, 19 Dec 2019 08:53:30 +0100

nitime (0.8.1-1) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * Team upload.
  * New upstream version, python3 only. (Closes: #937145)
  * Move package to salsa.debian.org/med-team

  [ Andreas Tille ]
  * Priotity optional
  * debhelper-compat 9 (leave level 9 for backwards compatibility)
  * Standards-Version: 4.4.1
  * Testsuite: autopkgtest-pkg-python
  * Secure URI in copyright format
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target
  * Remove trailing whitespace in debian/changelog
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database.
  * Remove unused paragraph from d/copryight

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 15 Dec 2019 15:32:08 +0100

nitime (0.7-2) unstable; urgency=high

  * Use mathjax sphinx.ext if no pngmath is available (Closes: #922256)
  * debian/control
    - boost policy to 4.3.0
    - remove obsolete X-Python*

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 21 Feb 2019 12:50:06 -0500

nitime (0.7-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - added python-{setuptools,pytest} into build-depends
  * debian/rules
    - skip slightly failing test_coherence_linear_dependence for now
      (see https://github.com/nipy/nitime/issues/150)

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 06 Jan 2017 15:18:05 -0500

nitime (0.6+git15-g4951606-1) unstable; urgency=medium

  * New upstream snapshot from rel/0.6-15-g4951606
    - contains fixes for compatibility with recent matplotlib etc
  * debian/control
    - boosted policy to 3.9.8

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 03 Aug 2016 22:42:06 -0400

nitime (0.6-1) unstable; urgency=medium

  * Fresh upstream bugfix release (Closes: #812700)
  * debian/patches -- dropped 2 patches previously picked up from upstream VCS
  * debian/watch -- adjusted for deprecated githubredir

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 07 Feb 2016 18:57:21 -0500

nitime (0.5-3) unstable; urgency=medium

  * Include upstream patch for modern matplotlib versions (Closes: #802524).

 -- Michael Hanke <mih@debian.org>  Wed, 30 Dec 2015 10:18:40 +0100

nitime (0.5-2) unstable; urgency=medium

  * Update maintainer email address.
  * Remove dependency on python-support, while upgrading to dh9
    (Closes: #786233).
  * Drop embedded javascript library in favor of a dependency on
    libjs-underscore.
  * Bump Standards-version, no changes necessary.

 -- Michael Hanke <mih@debian.org>  Mon, 24 Aug 2015 18:53:56 +0200

nitime (0.5-1) unstable; urgency=medium

  * New release
    - does not ship sphinxext/inheritance_diagram.py any longer
      (Closes: #706533)
  * debian/copyright
    - extended to cover added 3rd party snippets and updated years
  * debian/watch
    - updated to use githubredir.debian.net service
  * debian/patches
    - debian/patches/up_version_info_python2.6 for compatibility with
      python2.6 (on wheezy etc)

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 14 Jun 2014 07:34:18 -0400

nitime (0.4-2) unstable; urgency=low

  * Added graphviz to Build-Depends (Closes: #608908)
    Sorry that I have missed in -1 upload
  * Adding additional cleaning to assure pristine state for source
    package rebuilding (Closes: #643226, original report was about version.py
    being modified -- seems to be not the case any longer)

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 20 Jun 2012 09:01:35 -0400

nitime (0.4-1) unstable; urgency=low

  * New upstream release
    - fix commit for lazyimports pickling
    - compatible with scipy 0.10 API breakage (Closes: #671978)
  * debian/control:
    - added XS-Python-Version >= 2.6 (for squeeze backports)
    - reindented/untabified *Depends
    - boosted policy compliance to 3.9.3 (no further changes)
    - upcased Depends in ${python:Depends}.  Hopefully finally it
      (Closes: #614220)
  * debian/copyright:
    - updated years and fixed for DEP5 compliance
  * debian/rules
    - disable test_lazy_reload test (known to fail whenever ran by nosetest)
    - export HOME=$(CURDIR)/build just to avoid possible FTBFs
  * debian/watch
    - adjusted to fetch from tags

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 06 Jun 2012 16:04:24 -0400

nitime (0.3.1-1) unstable; urgency=low

  * Fresh bugfix release: addresses compatibility concerns allowing easy
    backporting
  * CP commit to fixup __version__ to report 0.3.1 instead of 0.4.dev

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 17 Aug 2011 17:35:17 -0400

nitime (0.3-1) UNRELEASED; urgency=low

  * Fresh upstream release
  * Adjusted debian/watch and added a rudimentary get-orig-source which
    uses uscan to fetch tarballs from github

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 15 Aug 2011 16:29:48 -0400

nitime (0.2.99-1) unstable; urgency=low

  * Pre-0.3 snapshot release
  * Boost policy compliance to 3.9.2 (no changes due)
  * Assure off-screen backend (Agg) for matplotlib while building docs

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 29 May 2011 21:48:41 -0400

nitime (0.2-2) unstable; urgency=low

  * Proper casing in ${python:depends} (Closes: #614220).
    Thanks Jakub Wilk for the report and for the fix

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 20 Feb 2011 09:40:41 -0500

nitime (0.2-1) unstable; urgency=low

  * Initial release (Closes: #600714)

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 22 Oct 2010 14:32:15 -0400
